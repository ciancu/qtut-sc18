\documentclass[a4paper,11pt]{article}
\usepackage{fullpage}
\include{preamble}
\usepackage{color}
\definecolor{blue}{rgb}{0,0,1}
\newcommand{\blue}[1]{{\color{blue} #1}}

\usepackage{eurosym}
\usepackage{hyperref}

% Comment/uncomment the next lines to remove the guidelines

\newcommand{\gl}[1]{\emph{#1}}
%\newcommand{\gl}[1]{}

\long\def\comment#1{}
\long\def\note#1{{\em #1 }}
\def\parah#1{\vspace*{0.08in} \noindent{\bf #1:}}
%\newcommand{\red}[1]{ \textcolor{red}{#1}}
%\newcommand{\red}[1]{}



\title{\bf Quantum Computing for Scientific Applications}

\date{}
\author{}

\begin{document}
\maketitle


\begin{description}

\item[Presenters:] \hfill \break

Costin Iancu - Lawrence Berkeley National Laboratory\\
Wibe de Jong - Lawrence Berkeley National Laboratory\\
Pavel Lougovski - Oak Ridge National Laboratory \\
Ojas D. Parekh - Sandia National Laboratories \\
Davide Venturelli - USRA, NASA Ames Quantum Laboratory \\
Jarrod McClean - Google \\
Nicholas Rubin - Rigetti \\
Andrew Cross - IBM

\item[Assistants:] \hfill \break
Dar Dahlen - University of California at Berkeley \\
Eugene Dumitrescu - Oak Ridge National Laboratory \\


\end{description}


\vfill


\clearpage

\begin{abstract}
Quantum computing is an emerging technology which promises to revolutionize 
many computational tasks. However, for a non-specialist it may appear that 
it is surrounded by a shroud of hype and misconception. The main goal of this 
tutorial is to de-mystify practical quantum computing and all its vital algorithmic 
aspects to a general audience of computer scientists with little to none prior 
knowledge of the subject. We plan to achieve this through a combination of lecture 
materials, demonstrations and hands on exercises delivered by quantum computing 
experts with extensive experience of public speaking and teaching from the 
Government Research Laboratories (DOE, NASA), industry (IBM, Google, Rigetti), and 
academia.  In particular, we aim to elucidate quantum computing use for scientific 
applications, covering the following areas: 1) quantum algorithm design;  
2) quantum programming toolkits; and 3) practical error mitigation for quantum 
algorithms. We will focus on the design and implementation of hybrid quantum-classical 
computational strategies including variational quantum eigensolver (VQE) and quantum 
approximate optimization algorithms (QAOA) in the context of quantum
chemistry, nuclear structure, telecommunication networks
and quantum field theory problems. We will discuss multiple practical ways to mitigate 
systematic coherent errors in the nascent quantum hardware, including general techniques 
such as randomized compilation. 
%To cover the spectrum of quantum programming, we 
%have hands on demonstrations given by industry participants, selected
%from the companies with quantum hardware offerings:
%IBM, Google and Rigetti. The content is designed for a general
%audience, with a 40\% beginner content and 40\% intermediate content. The presenters are involved with several ongoing quantum
%research projects (DOE, NASA) and have extensive speaking and teaching experience.
\\ \red{\bf Limit is 200 words.}
\end{abstract}


\clearpage

\section*{Tutorial Description}

Quantum computing is starting to transition from the lab to the field, moving from the pure
theoretical realm to delivering computational results from simulations
run on quantum processors. Although the available processors have only
few noisy qubits with limited connectivity, the scientific community is abuzz with excitement and
proof of concept simulations (quantum chemistry, machine learning,
optimization) that have been published in the last year. The societal
impact of quantum computing, if any, is expected to occur at the
decade time-scale and right now we are in a period where both
education and fast exploration of the design space are mandatory for
long term success and adoption. The goal of the tutorial is to
introduce the audience to quantum computing and cover  the aspects
necessary for developing quantum simulations for scientific
applications. The presenters are involved with several research
projects that aim to develop quantum algorithms and quantum testbeds
for  scientific computing and can provide an unique perspective on the
practicalities of quantum code development and its embedding in
existing large scale scientific simulations. We plan to provide hands
on experience programming existing ``commercial'' quantum hardware
offerings from IBM, Google and Rigetti.

\section{Tutorial Goals}
\begin{itemize}
\item Introduce quantum algorithm design for several domains (quantum
  chemistry, nuclear structure, telecommunication networks and quantum field theory) and describe the
  steps necessary to derive a quantum transformation  given the
  classical description of the problem.
\item Introduce programming models for existing hardware offerings
  (super-conducting qubits) and show examples of quantum algorithms.  
  Give a brief overview of existing hardware solutions and their characteristics.
\item Allow hands on experience with quantum software infrastructures.
\item Introduce the topic of error correction, which is mandatory for
  running on future, fault tolerant hardware.  
\end{itemize}
\section{Relevance}

Due to reaching the end of Moore's law and Dennard scaling, the High
Performance Computing community is looking at alternative models of
computation able to solve the challenging scientific problems of
interest. Given the current advances in processor design, quantum
computing is a very promising approach that may be ready to produce
high impact results in the decade time-frame. As deriving a quantum
transformation (algorithm) for a given science problem requires a
completely different mind set from the classical approaches, there is
an urgency to have scientists get familiar with  quantum
computing. This is recognized by the community at large and
there exists interest in any information about solving scientific
problems on actual quantum hardware: our tutorial will provide some of
these results. Besides the obvious differences in  algorithm design,
there exists another very significant difference between developing
software for classical and quantum computing. While in the classical
model layers of abstraction insulate scientists from the details of
the underlying hardware, designing a  quantum transformation than runs
on actual hardware requires a ``co-design'' process that considers the
algorithm in conjunction with the hardware. Our tutorial will
introduce the audience to this (non obvious) process of error
correction  and describe
possible solutions.  Overall, we believe the team of presenters is one
of the few in the world able to provide both a theoretical and
practical perspective of the great potential, as well as challenges,
of practical quantum computing for scientific computing in the next
decade. 

\section{Target Audience and Prerequisites}

There is large interest and curiosity in the scientific computing
community to understand the opportunities of quantum computing and we
expect a healthy audience that spans all domains. We expect little familiarity with quantum computing and
expect to introduce most concepts in an intuitive and understandable manner. Most of the software infrastructure
used in quantum computing relies of Python.
 
\section{Content Level} The topics we cover are algorithm design,
programming models and error mitigation. We plan a split 40\%
introduction, 40\% intermediate content, with a clear path from
concepts to intermediate. Even if the audience can't follow the
advanced material, we believe that understanding the intuition behind
and subtleties involved is very valuable. 

\section{Content}

The tutorial will start with a general overview of existing quantum
processor technologies and their associated programming models. We
will introduce i) the circuit model and native gate sets; ii)  qubit
connectivity, fidelity and
fault tolerance issues; iii) performance models;  as all influence algorithm design. Besides
super-conducting qubits, we plan to give a brief overview of competing
technologies such as cold atoms, trapped ions and photonics. In the next section, we plan to
introduce algorithmic case studies, including those from mathematics, quantum chemistry, nuclear
structure, telecommunication networks and quantum field theory and discuss their incorporation
into hybrid classical-quantum scientific simulations. This section
will also introduce the need for error correction and mitigation for
existing hardware. This includes a showcase of the challenges and the
approaches to compiler a variational quantum algorithm (VQE, QAOA)
onto real quantum processors. Next we plan to discuss error mitigation techniques
and cover algorithm specific techniques, as well as generic techniques
such as random compilation. In the last section of the tutorial we
will provide hands on demonstrations from several industry quantum
providers. Google will introduce their Cirq programming model and the
OpenFermion framework to develop  quantum algorithms to simulate
fermionic systems. Rigetti will introduce the  Forest programming
environment for developing hybrid simulations. IBM will demonstrate use of the established QISKit programming model and cloud API.

\section{Presenter Synergy and Preparation}

The presenters offer an unique combination of skills and are
already coordinating. Several of us are involved in
the DOE ASCR research projects in the areas of quantum
testbed development and quantum algorithms for science. On both fronts
we have regular informational and collaboration meetings. The
interaction with industry occurs also on both fronts. As the field is
young, this is the first time we are compiling the material for the
tutorial. The intention is to provide this tutorial at all DOE
facilities and continue to offer it at HPC conferences and events. We
plan to start in early Fall 2018 and to have had several presentations
by Supercomputing 2018.


\clearpage

\section*{Tutorial Outline}\label{sec:science}

\begin{enumerate}
\item Introduction to Quantum Hardware and Programming Models (1hr)
\begin{itemize}
\item Flavors of Quantum Hardware: superconducting qubits and 
  trapped ion
\item Analog and Digital quantum computing
\item The Circuit model for quantum programming
\item Metrics for correctness and performance: fidelity, quantum
  volume etc.
\end{itemize}
\item Quantum Algorithms (2 hrs)
\begin{itemize}
\item Variational Quantum Eigensolver (VQE) in Nuclear Physics
\item Phase estimation and VQE in Quantum Chemistry
\item Designing Quantum approximate optimization algorithms (QAOA)
\item Solving discrete optimization problems (e.g.graph partitioning)  that arise in scientific computing applications.
\end{itemize}
\item Error Correction (1hr)
\begin{itemize}
\item Types of errors and noise in quantum computing
\item Algorithm Specific Error Characterization and Mitigation
\item Algorithm Agnostic (generic) Techniques for Error Characterization and Mitigation
\end{itemize}
\item Overview of Available Quantum Development Environments and
  Processors (2 hrs)
\begin{itemize}
\item Using OpenFermion-Cirq towards practical quantum chemistry on
  quantum computers - Jarrod McClean (Google)
\item Using Forest  towards practical algorithms on
  quantum computers - Nicholas Rubin (Rigetti)
\item Using the QISKit SDK with OpenQASM on the IBM Q experience - Andrew Cross (IBM)
\item XACC - eXtreme-scale ACCelerator programming model: a programming language and hardware agnostic way to program quantum computers - Alex McCaskey (ORNL)
\end{itemize}
\end{enumerate}





\clearpage


\section*{Hands-on Exercises}

We are planning to provide attendees with hands-on experience using
quantum computing development environments. As at this time breadth is
more useful than depth, we will give priority to introducing the
environments from Google, Rigetti and IBM. Each company has a web
based interface for submitting quantum programs and the attendees can use
laptops and web browsers (Python). The initial intention is  to provide implementations
of the same algorithm on all platforms, but this may not be feasible
due to differences in native gate sets, different error
characteristics etc. Also, as the field evolves so rapidly, we will
give priority to showcasing the best attainable result on a given
platform, rather than the simplest common denominator. 

\clearpage

\section*{Curriculum Vitae}

\includepdf[pages=1-2]{iancu-resume}
\include{dejong-bio}
\includepdf[pages=1-5]{Jarrod_McClean_CV_Academic-1}
\includepdf[pages=1-2]{Parekh-Biosketch.pdf}
\includepdf[pages=1-5]{Nicholas_Rubin_CV}
\includepdf[pages=1-2]{biosketch_Lougovski.pdf}
\clearpage

\section*{SC18 Tutorial Digital Copy}

The DOE presenters agree to release their presentation notes for SC'17
Digital Copy. We are still determining the industry response.
\clearpage
\section*{Travel Support}

All participants are based in the continental US and we are requesting
travel support for four: two student assistants, Iancu and Venturelli.
\clearpage

\nocite{*}
\renewcommand\refname{\vskip -1cm}
\bibliographystyle{plain}
\bibliography{ref}





\end{document}

